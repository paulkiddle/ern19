const express = require('express');
const bodyParser = require('body-parser');
const Database = require('sqlite3').Database;
const sqlTag = require('sql-tag');
const {getData} = require('./stats/data');
const formidable = require('formidable');
const TransactionDatabase = require("sqlite3-transactions").TransactionDatabase;

const db = new Database('db.db');

const prom = fn => new Promise((resolve, reject) => fn((err, res) => {
    if (err) {
        reject(err)
    } else {
        resolve(res)
    }
}));

const sql = (...args) => {
    const o = sqlTag(...args);
    console.log(o.sql, o.values);
    return db.prepare(o.sql).bind(o.values);
}

const app = express();

app.set('view engine', 'pug');
app.set('views', 'views');

app.use('/static', express.static('static'));
app.use('/stats', express.static('stats'));
app.use('/npm', express.static('node_modules'));

app.get('/timer', (req, res) => {
    res.render('timer', {})
});
app.get('/', (req, res) => res.sendFile(require.resolve('./index.html')));
app.get('/stats', (req, res) => {
    getData(req.headers.host).then((data) => {
        res.render('stats',
                   {
                       title: 'ERN 2019',
                       teams: data.teams,
                       superlatives: data.superlatives
                   });
    });
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const groups = ["bot", "ent", "min", "zoo", "pal", "none"];

// Route for posting quiz results and getting
// assigned a group/storing it
// Params:
// ans - the group(s) the user matched the most on
// id - the client-generated ID
app.post('/', (req, res) => {
    const id = req.body.id;
    const answers = [].concat(...req.body.answers.map(a => a.split(',')))
                      .filter(a => groups.includes(a));
    const uid = req.body.uid;

    const count = {};
    let max = 0;
    for (const house of answers) {
        if (house in count) {
            count[house]++;
        } else {
            count[house] = 1;
        }

        max = Math.max(max, count[house]);
    }

    const vals = Object.keys(count).filter(k => count[k] === max);
    const answer = vals[Math.floor(Math.random() * vals.length)];

    const r = sql`INSERT INTO user (id, house, uid) VALUES(${id}, ${answer}, ${uid})`.run();
    console.log(r);

    if (uid !== '') {
        const updatePinScore = sql`UPDATE pin_score SET house = ${answer} WHERE uid = ${uid} and house = 'none'`.run();
        console.log(updatePinScore);
    }

    let breakdown = {};
    let total = Object.values(count).reduce((a, b) => a + b);
    Object.entries(count).map((d, i, a) => {
        return [d[0], Math.round((d[1] / total) * 100)];
    }).sort((a, b) => b - a).forEach((d) => {
        breakdown[d[0]] = d[1];
    });

    res.send({
                 house: answer,
                 breakdown: breakdown
             });
});

app.get('/count', async (req, res) => {
    const r = await prom(
        cb => sql`SELECT house, COUNT(*) as count FROM user GROUP BY house`.all(cb));
    const times = await prom(
        cb => sql`SELECT house, AVG(seconds) as averageTime, COUNT(*) as totalInsects, SUM(score) as score FROM pin_score GROUP BY house`.all(
            cb));

    //console.log(sql`SELECT * FROM pin_time`.all());
    const out = groups.map(house => ({
        ...(r.find(r => r.house == house) || {}),
        ...(times.find(r => r.house == house) || {})
    }))
    res.json(out);
});

app.post('/time', (req, res) => {
    const {time, house, score, uid} = req.body;
    const [mins, secs] = time.split(':');
    const seconds = (parseInt(mins, 10) * 60) + parseFloat(secs, 10);
    sql`INSERT INTO pin_score (house, seconds, score, uid) VALUES(${house}, ${seconds}, ${score}, ${uid})`.run();
    res.sendStatus(201);
});

app.get('/merge', (req, res) => {
    res.render('merge', {});
});

app.post('/merge', (req, res) => {
    let filePath = __dirname + '/merging.db';
    new formidable.IncomingForm()
        .parse(req)
        .on('fileBegin', (name, file) => {
            file.path = filePath;
        })
        .on('file', (name, file) => {
            let dbToMerge = new TransactionDatabase(db);

            dbToMerge.exec(`attach '${filePath}' as toMerge;`, (e) => {console.log(e)});

            dbToMerge.beginTransaction((err, trans) => {
                trans.run(
                    'insert or ignore into user select id, house, uid from toMerge.user;')
                trans.run(
                    'insert or ignore into pin_score select id, house, seconds, score, uid from toMerge.pin_score;')
                trans.commit((e) => {console.log(e)});
            });

            dbToMerge.exec('detach toMerge;', (e) => console.log(e));
        })
        .on('end', () => {
            res.redirect('/stats');
        });
});

const [, , port, ip] = process.argv;

app.listen(port || 3000, ip || '127.0.0.1',
           () => console.log(`Listening on http://${ip || 'localhost'}:${port || 3000}`));
