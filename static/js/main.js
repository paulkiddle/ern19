function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

function readableNumber(n) {
    const regex = /\B(?=(\d{3})+(?!\d))/g;

    return n.toString().replace(regex, ',');
}

// teams is defined in /static/data/teams.js

let id;

document.body.addEventListener('click', async e => {
    console.log(e.target);
    const button = e.target.closest('input');
    if (!button || !button.value) {
        return;
    }

    const next = button.closest('.question').nextElementSibling;
    if (!next) {
        submit();
    }
    const el = next || document.querySelector('.result');
    el.scrollIntoView({behavior: 'smooth'});
    //alert(await resp.text());
});

document.getElementById('quizForm').onsubmit = async (e) => {
    e.preventDefault();
    await submit();
    const el = document.querySelector('.result');
    el.scrollIntoView({behavior: 'smooth'});
};

async function submit() {
    const f = new URLSearchParams(new FormData(document.forms[0]));
    f.set('id', uuidv4());

    const resp = await fetch('/', {method: 'POST', body: f});
    let results = await resp.text();
    results = JSON.parse(results);
    const team = teams[results.house];

    let breakdownText = `Your best match is ${team.name} (${results.breakdown[results.house]}%)! Over ${readableNumber(
        team.digitised)} specimens from this collection have been digitised so far, and we've learned that...`;

    document.querySelector('.result-text').innerText =
        `You're in Team ${team.name}!`;
    document.querySelector('.result-breakdown').innerText = breakdownText;

    const blurbLineTag = '<p class="blurb-line">';
    document.querySelector('.result-blurb').innerHTML =
        blurbLineTag + team.questions.join('</p>' + blurbLineTag) + '</p>';

    fetch(`/static/images/${results.house}.svg`)
        .then((res) => res.text())
        .then((src) => {
            const img = (new window.DOMParser()).parseFromString(src,
                                                                 "text/xml").documentElement;
            img.classList.add('team-svg');
            const cur = document.querySelector('.team-svg');
            cur.parentElement.replaceChild(img, cur);
        });

    //startQR();
}