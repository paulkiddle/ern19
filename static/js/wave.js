function getWavePath(containerWidth, containerHeight) {
    let waveHeight = containerHeight / 50;
    let nWaves = 2;
    let waveLength = (containerWidth * 2) / (2 * nWaves);
    let singleWave = `c 0,0 ${waveLength / 2},${waveHeight} ${waveLength},0 ` +
                     `c 0,0 ${waveLength / 2},-${waveHeight} ${waveLength},0`;
    let svgPath = `M 0,${waveHeight/2} `;
    for (i = 0; i < nWaves; i++) {
        svgPath += singleWave;
    }
    svgPath += `v 0,${containerHeight} h -${containerWidth * 2},0 Z`;

    return svgPath
}

function getFill(container, containerHeight) {
    let score = container.find('input.team-score').val();
    let fillLevel = Math.ceil(containerHeight - (containerHeight * (score / 800)));
    return fillLevel;
}

function resize() {
    const ratio = 921 / 605;
    let cols = $('.col-2.team-col');
    let counterContainer = cols.find('.counter-inner');
    let divWidth = counterContainer.outerWidth(true);
    let divHeight = counterContainer.outerHeight(true);
    let containerSize = {
        width: divWidth,
        height: Math.min(divHeight, divWidth * ratio)
    };
    let vOffset = ((divHeight - containerSize.height) / 2);

    let wavePath = getWavePath(containerSize.width, containerSize.height);

    let waveSvg = cols.find('.wave-shape');
    waveSvg.attr('width', (containerSize.width * 2) + 'px');
    waveSvg.attr('height', (containerSize.height) + 'px');
    waveSvg.attr('viewBox', '0 0 ' + (containerSize.width * 2) + ' ' +
                            (containerSize.height));
    waveSvg.children('path').attr('d', wavePath);

    let endTranslate = `translate(-${containerSize.width}px, 0)`;

    $.keyframe.define([
                          {
                              name: 'waveAction',
                              '0%': {'transform': `translate(0, 0)`},
                              '100%': {'transform': endTranslate}
                          }]);


    waveSvg.each((i, e) => {
        $(e).playKeyframe([{
            name: 'waveAction',
            duration: `${Math.random() * (3 - 1) + 1}s`,
            timingFunction: 'linear',
            iterationCount: 'infinite'
        }]);
    });

    cols.each((i, e) => {
        e = $(e);
        let fillLevel = getFill(e, containerSize.height);

        $.keyframe.define([{
            name: `fillAction-${i}`,
            '0%': {'transform': `translate(0, ${containerSize.height}px)`},
            '100%': {'transform': `translate(0, ${fillLevel + vOffset}px)`}
        }]);

        e.find('.counter-fill').playKeyframe([{
            name: `fillAction-${i}`,
            iterationCount: '1',
            timingFunction: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
            duration: '4s',
            fillMode: 'forwards'
        }]);
    });
}

$(() => {
    resize();
    $(window).resize(() => {
        console.log('resizing');
        window.location.href = window.location.href;
    })
});