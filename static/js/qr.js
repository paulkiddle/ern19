async function startQR() {
    var qr = new QrCode();
    qr.callback = (er, res) => {
        if (res && res.result) {
            document.querySelector('.result-text').innerText = res.result;
            fetch('/scan', {
                method: 'POST',
                body: JSON.stringify({id, scan: res.result}),
                headers: {'Content-Type': 'application/json'}
            });
        }
    };

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');
    var canvas = document.createElement('canvas');
    // document.querySelector('svg').style.left = video.offsetLeft + 'px';
    // document.querySelector('svg').style.height = video.offsetHeight + 'px';
    // if you want to preview the captured image,
    // attach the canvas to the DOM somewhere you can see it.
    //convert to desired file format

    // Get access to the camera!
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        return;
    } // Don't know how to get the camera

    //    Not adding `{ audio: true }` since we only want video now
    const stream = navigator.mediaDevices.getUserMedia({video: true});

    // Ask politely for the user to give us their camera

    //video.src = window.URL.createObjectURL(stream);
    // Todo: Handle rejection
    video.srcObject = await stream;
    video.play().then(() => {
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
    });

    setInterval(() => {
        var ctx = canvas.getContext('2d');
        //draw image to canvas. scale to target dimensions
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        var dataURI = canvas.toDataURL('image/jpeg');
        qr.decode(dataURI);
    }, 1000);

}