function update(timeInput, time) {
    const mins = Math.floor(time / 60000);
    const secs = time / 1000 - mins * 60;

    timeInput.value =
        String(mins).padStart(2, '0') + ':' + secs.toFixed(1).padStart(4, 0);
}

function step(timeInput, timestamp = performance.now()) {
    if (!timeInput.start) timeInput.start = timestamp;
    update(timeInput, timestamp - timeInput.start);

    timeInput.timeoutId = setTimeout(() => timeInput.timeoutId =
        window.requestAnimationFrame((x) => step(timeInput, x)), 100);
}

function startt(timeInput) {
    timeInput.started = true;
    step(timeInput);
}

function stop(timeInput) {
    update(timeInput, performance.now() - timeInput.start);
    timeInput.start = null;
    cancelAnimationFrame(timeInput.timeoutId);
    clearTimeout(timeInput.timeoutId);
    timeInput.started = false;
}

function timer(timeInput) {
    if (!timeInput.started) {
        startt(timeInput);
    } else {
        stop(timeInput);
    }
}

async function getUID() {
    let wordList = await fetch('/static/data/words.json').then((res) => res.text());
    wordList = JSON.parse(wordList);
    let adjective = wordList.adjectives[Math.floor(
        Math.random() * (wordList.adjectives.length))];
    let animal = wordList.animals[Math.floor(Math.random() * (wordList.animals.length))];
    return adjective.toLowerCase() + ' ' + animal.toLowerCase();
}

function getDigiTime(timeInput) {
    let timeTaken = timeInput.value;
    if (timeTaken === null) {
        return 0;
    }
    const [mins, secs] = timeTaken.split(':');
    const seconds = (parseInt(mins, 10) * 60) + parseFloat(secs, 10);
    let totalTime = Math.round((seconds * 80000000) / (60 * 60 * 5 * 260));
    return `It would take you about ${totalTime} years to digitise all 80 million specimens by yourself!`;
}

$(() => {
    $('.timer-group .timer-button').click((event) => {
        let btn = $(event.target);
        let timerGroup = btn.parents('.timer-group');
        btn.toggleClass('stopped');
        btn.toggleClass('started');
        timerGroup.find('.person-id').removeClass('show');
        timerGroup.find('.stats').toggleClass('show');
        let timeInput = timerGroup.find('#time')[0];
        timerGroup.find('#total-time').text(getDigiTime(timeInput));
        timer(timeInput);
    });

    $('.timer-group #group button').click(
        async (event) => {
            let btn = $(event.target);
            let timerGroup = btn.parents('.timer-group');
            let timeInput = timerGroup.find('#time')[0];
            let scoreInput = timerGroup.find('#score')[0];
            let uid = await getUID();

            const time = timeInput.value;
            const body = JSON.stringify({
                                            time,
                                            house: btn.val(),
                                            score: scoreInput.value,
                                            uid: uid
                                        });
            await fetch('/time', {
                method: 'POST',
                body,
                headers: {'Content-Type': 'application/json'}
            });
            timeInput.value = "00:00.0";
            scoreInput.value = 1;
            timerGroup.find('#uid').text(uid);
            timerGroup.find('.person-id').addClass('show');
        }
    )
});