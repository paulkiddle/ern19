const teams = {
    "pal": {
        "name": "Palaeontology",
        "digitised": 430000,
        "questions": [
            "More palaeontology specimens were collected on the <strong>12th</strong> of the month than on any other day",
            "<strong>46.85%</strong> of the Museum's palaeontology records have been <strong>imaged</strong>",
            "More palaeo specimens have been collected from the <strong>Western Atlantic Ocean</strong> than any other marine body",
            "Almost 190,000 of our palaeontological records were collected in the <strong>UK</strong>",
            "More palaeo specimens were collected in 1971 than in any other year, while <strong>'My Sweet Lord'</strong> by George Harrison topped the charts",
            "5,639 of our palaeontology records were collected in October, when <strong>Crocuses</strong> bloom"
        ]
    },
    "min": {
        "name": "Mineralogy",
        "digitised": 365000,
        "questions": [
            "More mineralogy specimens were collected on the <strong>7th</strong> of the month than on any other day",
            "Only <strong>1.26%</strong> of our mineralogy records have been <strong>imaged</strong>",
            "More mineralogy specimens have been collected from the <strong>Northeast Atlantic Ocean</strong> than any other marine body",
            "Almost 14,000 of our mineralogy specimens were collected in <strong>the USA</strong>",
            "More mineralogy specimens were collected in 1972 than in any other year, while <strong>'Amazing Grace'</strong> topped the charts",
            "3,253 of our mineralogy records were collected in May, when <strong>Deplhinium</strong> blooms"
        ]
    },
    "ent": {
        "name": "Entomology",
        "digitised": 1500000,
        "questions": [
            "More entomology specimens were collected on the <strong>20th</strong> of the month than on any other day",
            "<strong>62.29%</strong> of the museum's entomology records have <strong>images</strong>",
            "<strong>Hardly any</strong> of our entomology records show that they were collected from a marine environment",
            "Over 40,000 of our entomology records were collected in <strong>Tanzania</strong>",
            "More entomology specimens were collected in 2012 than in any other year, while <strong>'Somebody I Used to Know'</strong> by Goyte topped the charts",
            "105,100 of our entomology records were collected in July, when <strong>Lilies</strong> bloom"
        ]
    },
    "bot": {
        "name": "Botany",
        "digitised": 770000,
        "questions": [
            "More botany records were collected on the <strong>8th</strong> of the month than on any other day",
            "Only <strong>32.87%</strong> of the museum's botany records have <strong>images</strong>",
            "More botanical specimens have been collected from the <strong>English Channel</strong> than any other marine body",
            "Almost 16,000 of our botany records were collected in <strong>Brazil</strong>",
            "More botany specimens were collected in 1938 than in any other year, <strong>before the singles charts began</strong>",
            "54,700 of our botany records were collected in July, when <strong>Cornflowers</strong> bloom"
        ]
    },
    "zoo": {
        "name": "Zoology",
        "digitised": 1300000,
        "questions": [
            "More zoology specimens were collected on the <strong>1st</strong> of the month than on any other day",
            "<strong>59.91%</strong> of the Museum's zoology records have <strong>images</strong>",
            "More zoology specimens have been collected in the <strong>Indian Ocean</strong> than any other marine body",
            "Around 35,000 of our zoology records were collected in <strong>South Africa</strong>",
            "More zoology specimens were collected in 1965 than in any other year, while <strong>'Tears'</strong> by Ken Dodd topped the charts",
            "42,220 of our zoology records were collected in August, when <strong>Dahlias</strong> bloom"
        ]
    }
};