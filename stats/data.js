const fetch = require('node-fetch');

const DEV = false;

function randint(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

const collectives = {
    'bot': 'botanist',
    'ent': 'entomologist',
    'min': 'mineralogist',
    'zoo': 'zoologist',
    'pal': 'palaentologist'
};


function rawJson(host) {
    if (!DEV) {
        try {
            let URL = '/count';
            URL = URL.startsWith('http') ? URL : 'http://' + host + URL;
            return fetch(URL).then(response => response.json());
        }
        catch (e) {
        }
    }

    let example_json = [
        {
            "house": "bot"
        },
        {
            "house": "ent"
        },
        {
            "house": "min"
        },
        {
            "house": "zoo"
        },
        {
            "house": "pal"
        }
    ].map((d) => {
        d.count = randint(0, 100);
        d.score = randint(0, 100);
        d.averageTime = randint(20, 200);
        return d;
    });

    return new Promise((resolve, reject) => {
        resolve(example_json);
    })
}

function getData(host) {
    return rawJson(host).then((teams) => {
        teams = teams.filter((d) => d.house != null && d.house !== 'none');
        teams.map((d) => {
            d.collective = collectives[d.house];
            d.averageTime = d.averageTime || -1;
            d.score = d.score || 0;
            d.totalInsects = d.totalInsects || 0;
            if (d.averageTime < 0) {
                d.totalCollDigi = -1;
            } else {
                d.totalCollDigi = Math.round(
                    ((d.averageTime * 80000000) / (60 * 60 * 5 * 260))/ d.count);
            }
            return d;
        });
        let fastest = teams.filter((d) => d.totalCollDigi >= 0);
        fastest = fastest.length > 0 ? fastest.reduce((a, b) => a.averageTime <= b.averageTime ? a : b) : {'house': 'nobody', 'averageTime': 0};

        let superlatives = {
            biggest: teams.reduce((a, b) => a.count >= b.count ? a : b),
            winning: teams.reduce((a, b) => a.score >= b.score ? a : b),
            fastest: fastest,
            mostInsects: teams.reduce((a, b) => a.totalInsects >= b.totalInsects ? a : b)
        };
        let data = {
            teams: teams,
            superlatives: superlatives
        };
        return data;
    })
}

module.exports = {
    'rawData': rawJson,
    'getData': getData
};