const fs = require('fs');
const path = require('path');
const Database = require('sqlite3').Database;
const db = new Database('db.db', { verbose: console.log });

const args = process.argv.slice(2);
const files = args.length ? args : fs.readdirSync(__dirname)
                .filter(f => /\.sql$/.test(f))
                .sort()
                .map(f => path.join(__dirname,f))

for(const arg of files) {
  const migration = fs.readFileSync(arg, 'utf8');
  db.exec(migration);
}
