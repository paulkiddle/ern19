CREATE TABLE if not exists pin_score (
	id		INTEGER		PRIMARY KEY	AUTOINCREMENT,
	house	VARCHAR(3),
	seconds	DECIMAL(6,2),
	score	INTEGER,
	uid     VARCHAR(100)
);